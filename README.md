To run the code please run 'run.sh' with the input file name (currently a sample 'itcont.txt' is used as the input file in run.sh ). The output filenames : 'median_by_zip.txt' and 'median_by_date.txt' are used in the 
'run.sh' currently.

The code is written using Python 2.7 and have used Pandas library. 
