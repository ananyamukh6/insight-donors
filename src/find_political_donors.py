import heapq
import time
import pandas as pd
import pdb, sys
import numpy as np


class RunMedian(object):
	#A class to keep track of running median using 2 heaps
	#usage: rm = RunMedian()
	#med = rm.push(val)
	def __init__(self):
		self.greater_min_heap = []
		self.lesser_max_heap = []
		self.median = None
	def push(self, num):
		if self.median is None: #this is the first push
			heapq.heappush(self.lesser_max_heap,-float(num))  #convert the number to a negative to simulate a max heap using heapq, which implements a min heap
			self.median = num
			return self.median
		#push the new number in the appropriate heap
		if num >= self.median:
			heapq.heappush(self.greater_min_heap,float(num))
		else:
			heapq.heappush(self.lesser_max_heap,-float(num))
		self.rebalance()  #rebalance the heaps if needed
		self.median = self.calcmedian()
		return self.median
	def rebalance(self):
		heaplendiff = len(self.greater_min_heap) - len(self.lesser_max_heap)
		assert abs(heaplendiff) <= 2
        #pop an element out of the source heap, change its sign and push it into the other heap
		if heaplendiff == 2:
			tmp = heapq.heappop(self.greater_min_heap)
			heapq.heappush(self.lesser_max_heap, -tmp)
		elif heaplendiff == -2:
			tmp = heapq.heappop(self.lesser_max_heap)
			heapq.heappush(self.greater_min_heap, -tmp)
	def calcmedian(self):
		heaplendiff = len(self.greater_min_heap) - len(self.lesser_max_heap)
		assert abs(heaplendiff) <= 1
		if heaplendiff == 0:
			return (self.greater_min_heap[0]-self.lesser_max_heap[0])/2.0
		else:
			return self.greater_min_heap[0] if heaplendiff==1 else -self.lesser_max_heap[0]
			
def readdata(filelist):
    #filelist: can be the name of a single file or a list of files
	tt = time.time()
	if (type(filelist) != type([])):
		filelist = [filelist]
	data_list = [] #list of dataframes
	for name in range(len(filelist)):
		filename = filelist[name]
		data = pd.read_csv(filename, sep = "|", dtype=object, header = None)
		tmp = data.iloc[:, [0,10,13,14,15]]
		tmp.columns = ['CMTE_ID', 'ZIP_CODE', 'TRANSACTION_DT', 'TRANSACTION_AMT', 'OTHER_ID']
		data_list.append(tmp) #selects the columns cmte_id, zip_code, transaction_dt, transaction_amt, other_id
	ttt = time.time()
	data = pd.concat(data_list, ignore_index=True) #concatenates the 4 dataframes into 1
	print 'TIME TO CONCAT DATA', time.time() - ttt
	#print (data.head(20))
	print 'TIME TO READ DATA', time.time() - tt
	return data #returns 1 dataframe with only the 5 columns of interest

def clean(data, byzip):
	tt = time.time()
	data = data[data.OTHER_ID.isnull()] #filters the row with empty other_id
	data = data[data.CMTE_ID.notnull()]
	data = data[data.TRANSACTION_AMT.notnull()]
	if not byzip:
		data = data[data.TRANSACTION_DT.notnull()]
	if byzip:
		data = data[data.ZIP_CODE.notnull()]
		data = data[data['ZIP_CODE'].str.len()>=5]  #remove rows with short zip codes
		data.ZIP_CODE = data.ZIP_CODE.str[0:5]
	data = data[['CMTE_ID', 'ZIP_CODE' if byzip else 'TRANSACTION_DT', 'TRANSACTION_AMT']]
	data[['TRANSACTION_AMT']] = data[['TRANSACTION_AMT']].astype(float)
	print 'TIME TO CLEAN DATA', time.time() - tt
	return data

def save(data, csvname, index=True):  #this function saves to csv without a newline at the end
	data1 = data.iloc[0:len(data)-1]
	data2 = data.iloc[[len(data)-1]]
	data1.to_csv(csvname, sep='|', encoding='utf-8', header= False, index=index)
	data2.to_csv(csvname, sep='|', encoding='utf-8', header= False, index=index, mode='a',line_terminator="")
	
def to_dmy_mdy(dt):
	return dt[2:4] + dt[0:2] + dt[4:]
def process_date(data, date_out_file):
	cleandata = clean(data, False)
	grp = cleandata[['CMTE_ID', 'TRANSACTION_DT', 'TRANSACTION_AMT']].groupby(['CMTE_ID', 'TRANSACTION_DT'])
	df = pd.concat([grp.median().round(), grp.count(), grp.sum()], axis=1).astype(int) #TODO: decide if total sum is int (round it) or float?
	#df.to_csv('medianvals_by_date.txt', sep='|', header=False)  #this command leaves a newline at the end, hence using 'save' function to prevent the last newline
	df.reset_index(inplace=True)
	df.TRANSACTION_DT = df.TRANSACTION_DT.apply(to_dmy_mdy)
	df = df.sort_values(['CMTE_ID', 'TRANSACTION_DT'], ascending=[True, True])
	df.TRANSACTION_DT = df.TRANSACTION_DT.apply(to_dmy_mdy)
	save(df, date_out_file, False)
	
def process_zip(data, zip_out_file, debug=False):
	cleandata = clean(data, True)
	state_dict = {}
	out_col_names = ['CMTE_ID', 'ZIP_CODE', 'RUNNING_MEDIAN', 'NUM_TRANSACTIONS', 'TOTAL_AMOUNT']
	out_df = pd.DataFrame([], columns=out_col_names)
	out_df[['RUNNING_MEDIAN', 'NUM_TRANSACTIONS', 'TOTAL_AMOUNT']] = out_df[['RUNNING_MEDIAN', 'NUM_TRANSACTIONS', 'TOTAL_AMOUNT']].astype(int)
	if debug:
		state_dict_bruteforce = {}
	for idx, record in cleandata.iterrows():
		tt = time.time()
		key = (record.CMTE_ID, record.ZIP_CODE)
		amt = int(round(float(record.TRANSACTION_AMT))) #Round amount to nearest dollar
		if key in state_dict:
			rm, oldmedian, num_transactions, totamount = state_dict[key]
		else:
			rm = RunMedian()
			num_transactions = 0
			totamount = 0
		med = int(round(rm.push(amt)))  #round median values to nearest dollar value
		if debug:
			state_dict_bruteforce[key] = state_dict_bruteforce.get(key, []) + [amt]
			med_brute = int(round(np.median(state_dict_bruteforce[key])))
			assert med == med_brute, 'Brute force median calculation did not match the heap calculated	 median'
		state_dict[key] = (rm, med, num_transactions+1, totamount+amt)
		row = pd.DataFrame([[record.CMTE_ID, record.ZIP_CODE, med, num_transactions+1, totamount+amt]], columns=out_col_names)
		out_df = out_df.append(row)
        if debug:
            if idx%1000 == 0:
                print idx, len(cleandata), time.time() - tt
            if idx%10000 == 0:
                tt = time.time()
                save(out_df, zip_out_file, False)
                print 'Save time', time.time() - tt
	
	save(out_df, zip_out_file, False)
	


def process(filename, zip_out_file, date_out_file):
    data = readdata(filename)
    process_zip(data, zip_out_file, False)
    process_date(data, date_out_file)
    

if __name__ == '__main__':
    process(sys.argv[1], sys.argv[2], sys.argv[3])
    
			
#path = "../data/indiv18/by_date/"
#data = readdata('../input/itcont_2018_20170908_20171103.txt')
#data = readdata([path+'itcont_2018_20120521_20170529.txt', path+'itcont_2018_20170530_20170907.txt', path+'itcont_2018_20170908_20171103.txt', path+'itcont_2018_invalid_dates.txt'])
#process_date(data)
#process_zip(data, False)


#Testing running median		
#from numpy import median
#datastream = [3,1,5,7,3,5,1,4,5,2,5]
#rm = RunMedian()
#for i,v in enumerate(datastream):
#	print v, rm.push(v), median(datastream[:i+1]) 
